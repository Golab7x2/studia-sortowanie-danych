package pl.gfg;

import java.util.Scanner;

public class Main {

    public static void main (String[] args) {

        System.out.println("Podaj nazwę pliku wraz z rozszerzeniem: ");
        Scanner readFilename = new Scanner(System.in);
        String filename = readFilename.nextLine();
        new DataSort(filename);
        System.out.println("Naciśnij ENTER");
        readFilename.nextLine();
    }

}
