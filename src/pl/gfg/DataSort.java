package pl.gfg;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class DataSort {

    private String filename;

    public DataSort(String filename) {
        this.filename = filename;

        List<String> results = this.sortData(this.loadFile());
        this.saveResults(results);
        this.countMeanValue(results);
    }

    /**
     *
     * Wczytywanie pliku
     */
    private BufferedReader loadFile() {

        System.out.println("Wczytuję dane...");
        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new FileReader(filename));
        }
        catch (FileNotFoundException e) {
            System.out.println("Niepoprawna nazwa pliku!");
        }

        return reader;
    }

    /**
     * sortowanie danych z pliku
     *
     */
    private List<String> sortData(BufferedReader reader) {

        System.out.println("Sortuję...");

        List<String> dataList = new ArrayList<>();
        String line;
        String subLine;
        String date = "null";
        String mlValue = "null";
        String saveLine;

        try {
            while ( (line = reader.readLine()) != null) {
                line = line.trim().toUpperCase();
                if (line.length() > 2) {
                    subLine = line.substring(0, 3);
                    if (subLine.equals("DAT")){
                        date = reader.readLine();
                        date = date.substring(0, 22);
                    }
                    else if (subLine.equals("ML ")){
                        mlValue = line.substring(0, 11);
                    }

                    if (!mlValue.equals("null") && !date.equals("null")) {
                        saveLine = mlValue + " " + date;
                        dataList.add(saveLine);
                        //System.out.println(saveLine);
                    }

                    mlValue = "null";

                }

            }
        }
        catch (IOException e) {
            System.out.println("Błąd sortowania!");
        }

        return dataList;
    }

    /**
     * zapis surowych danych do pliku z wynikami
     *
     */
    private void saveResults(List<String> results) {

        System.out.println("Zapis surowych danych...");

        int filenameLength = this.filename.length();
        this.filename = this.filename.substring(0, filenameLength-4);
        try {
            PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(filename + "-SUROWE.txt")));
            writer.println("Opracował: Jakub Gołębiewski");
            for (String r : results) {
                writer.println(r);
            }
            writer.close();
        }
        catch (IOException e) {
            System.out.println("Błąd zapisu pliku!");
        }
    }

    /**
     *
     * obliczenie i zapis uśrednionych danych do pliku wynikowego
     */

    private void countMeanValue(List<String> results) {

        System.out.println("Zapis wartości średnich...");

        int iterator = 0;
        String day = "null";
        double mlValue = 0.0;
        double meanValue;
        String date = "";
        List<String> meanResults = new ArrayList<>();

        for (String r : results) {
            if (day.equals(r.substring(20, 33))) {
                mlValue += Double.valueOf(r.substring(7, 10));
                iterator++;
            }
            else {
                if (iterator != 0) {
                    meanValue = mlValue / iterator;
                    meanResults.add(date + "     " + meanValue);
                }

                date = r.substring(12, 33);
                day = r.substring(20, 33);
                mlValue = Double.valueOf(r.substring(7, 10));
                iterator = 1;
            }

        }
        //oblicza ostatnią wartość, która nie została "wykryta" przez else
        meanValue = mlValue / iterator;
        meanResults.add(date + "     " + meanValue);

        try {
            PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(this.filename + "-USREDNIONE-DLA-DANEGO-TRZESIENIA.txt")));
            writer.println("Opracował: Jakub Gołębiewski");
            for (String mr : meanResults) {
                writer.println(mr);
            }
            writer.close();
        }
        catch (IOException e) {
            System.out.println("Błąd zapisu pliku!");
        }

    }

}
